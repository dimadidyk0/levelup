import React, { useState, useCallback } from 'react';
import styles from './SelfStorageForm.module.css';

type Props = {
  onSubmit: (args: { title: string }) => void;
}

export default function SelfStorageForm({ onSubmit }: Props) {
  const [title, setTitle] = useState('');
  const handleChange = useCallback((e: React.FormEvent<EventTarget>) => {
    const target = e.target as HTMLInputElement;
    setTitle(target.value);
  }, [setTitle]);

  const handleSubmit = useCallback((e: React.FormEvent) => {
    e.preventDefault();
    onSubmit({ title });
  }, [onSubmit, title]);

  return (
    <form onSubmit={handleSubmit}>
      <fieldset className={styles.field}>
        <label>
          Title
          <input className={styles.input} value={title} onChange={handleChange} name={'title'} />
        </label>
      </fieldset>

      <button type="submit">Submit</button>
    </form>
  )
}
