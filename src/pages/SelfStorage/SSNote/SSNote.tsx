import styles from './SSNote.module.css';

type Props = {
  title: string;
  id: string;
};

export default function SSNote({ title }: Props) {
  return (
    <div className={styles.root}>
      <img alt={title} src={'https://picsum.photos/40/40'} className={styles.img} />
      <h4>{title}</h4>
    </div>
  )
}
