import { useState, useCallback } from 'react';
import Button from '../../components/ui/Button/Button';
import Layout from '../../components/ui/Layout/Layout';
import Loader from '../../components/ui/Loader/Loader';
import Modal from '../../components/ui/Modal/Modal';
import { useFetchSelfStorage } from '../../hooks/api/useSelfStorageAPI';
import SelfStorageForm from './Form/SelfStorageForm';
import styles from './SelfStorage.module.css';
import SSNote from './SSNote/SSNote';

export function SelfStorage() {
  const [isAddModalOpen, setIsOpen] = useState(false);
  const toggleModal = useCallback(() => setIsOpen(prev => !prev), [setIsOpen]);

  const { data, isLoading } = useFetchSelfStorage();

  if (isLoading) {
    return <Loader />
  }

  const handleSubmit = ({ title }: { title: string }) => {
    console.log(title);
  }


  return (
    <>
      <Layout>
        <div className={styles.root}>
          <div className={styles.header}>
            <h1>Self Storage Notes</h1>
            <Button onClick={toggleModal}>Add note</Button>
          </div>

          {data.map(note => (
            <SSNote key={note?.id} {...note} />
          ))}
        </div>
      </Layout>
      <Modal isOpen={isAddModalOpen} onRequestClose={toggleModal}>
        <SelfStorageForm onSubmit={handleSubmit} />
      </Modal>
    </>
  );
}

export default SelfStorage;