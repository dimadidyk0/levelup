import MobileHeader from '../../components/MobileHeader/MobileHeader';
import Layout from '../../components/ui/Layout/Layout';
import styles from './Todo.module.css';

export function Todo() {
  return (
    <Layout>
      <div className={styles.root}>
        <MobileHeader title={'TODO'} />
      </div>
    </Layout>
  );
}

export default Todo;