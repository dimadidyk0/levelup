import Character from '../../components/Character/Character';
import Layout from '../../components/ui/Layout/Layout';
import styles from './Profile.module.css';

export function Profile() {
  return (
    <Layout>
      <div className={styles.root}>
        <Character />
      </div>
    </Layout>
  );
}

export default Profile;