import Layout from '../../components/ui/Layout/Layout';
import styles from './Main.module.css';

export function Main() {
  return (
    <Layout>
      <div className={styles.root}>
        root
      </div>
    </Layout>
  );
}

export default Main;