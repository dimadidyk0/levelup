import Layout from '../../components/ui/Layout/Layout';
import styles from './SelfStorageAdd.module.css';

export function SelfStorageAdd() {
  return (
    <Layout>
      <div className={styles.root}>
        <h1>Self Storage Add Form</h1>
      </div>
    </Layout>
  );
}

export default SelfStorageAdd;
