import styles from './MobileHeader.module.css';

type Props = {
  title: string;
  children?: React.ReactNode;
};

export default function MobileHeader({ title, children }: Props) {
  return (
    <div className={styles.root}>
      <h1 className={styles.title}>{title}</h1>
      <div className={styles.right}>{children}</div>
    </div>
  )
}
