import { NavLink } from 'react-router-dom'
import { ReactComponent as SettingsIcon } from '../../../../assets/icons/settings.svg';
import { ReactComponent as TodoIcon } from '../../../../assets/icons/todo.svg';
import { ReactComponent as FinanceIcon } from '../../../../assets/icons/finance.svg';
import { ReactComponent as CalendarIcon } from '../../../../assets/icons/calendar.svg';
import styles from './BottomNavigation.module.css';
import classNames from 'classnames';
import { FINANCE, SETTINGS, CALENDAR, TODO } from '../../../../constants/routes';

const links = [
  {
    translationKey: 'Calendar',
    Icon: CalendarIcon,
    link: CALENDAR,
  },
  {
    translationKey: 'ToDo List',
    Icon: TodoIcon,
    link: TODO,
  },
  {
    translationKey: 'Finance',
    Icon: FinanceIcon,
    link: FINANCE,
  },
  {
    translationKey: 'Settings',
    Icon: SettingsIcon,
    link: SETTINGS,
  },
]

export default function BottomNavigation() {
  return (
    <nav className={styles.root}>
      {links.map(({
        translationKey,
        Icon,
        link,
      }) => (
        <NavLink
          className={({ isActive }) => classNames(styles.link, isActive && styles.active)}
          to={link}
          key={translationKey}
        >
          <Icon />
          {translationKey}
        </NavLink>
      ))}
    </nav>
  )
}
