import { render, screen } from "@testing-library/react";
import Layout from "./Layout";

test("renders children correctly", () => {
  const text = 'Layout';
  render(<Layout>{text}</Layout>);
  const linkElement = screen.getByText(text);

  expect(linkElement).toBeInTheDocument();
});
