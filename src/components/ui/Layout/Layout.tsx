import React from 'react';
import { useIsDesktop } from '../../../hooks/useResponsive';
import styles from './Layout.module.css';
import Sidebar from './Sidebar/Sidebar';
import BottomNavigation from './BottomNavigation/BottomNavigation';

type Props = {
  children: React.ReactNode;
};

export default function Layout({ children }: Props) {
  const isDesktop = useIsDesktop();

  return (
    <div className={styles.root}>
      {isDesktop && <Sidebar />}

      <div className={styles.container}>
        {children}
      </div>

      {!isDesktop && <BottomNavigation />}
    </div>
  )
};
