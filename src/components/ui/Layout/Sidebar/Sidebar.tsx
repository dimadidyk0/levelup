import { NavLink } from 'react-router-dom'
import { PROFILE, ROOT, SELF_STORAGE, TODO } from '../../../../constants/routes'
import styles from './Sidebar.module.css';

export default function Sidebar() {
  return (
    <aside className={styles.sidebar}>
      <nav className={styles.nav}>
        <NavLink className={styles.link} to={ROOT}>Home</NavLink>
        <NavLink className={styles.link} to={PROFILE}>Profile</NavLink>
        <NavLink className={styles.link} to={TODO}>TODO</NavLink>
        <NavLink className={styles.link} to={SELF_STORAGE}>Self Storage</NavLink>
      </nav>
    </aside>
  )
}
