import React from 'react';
import ReactDOM from 'react-dom';

const rootSelector = '#modal';
let rootElement = null;

class ModalPortal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  initRootContainer = () => {
    console.log(rootElement);
    if (!rootElement) {
      const modalRootElement = document.createElement('div');
      modalRootElement.id = 'modal-inner';
      document.querySelector(rootSelector).appendChild(modalRootElement);
      rootElement = modalRootElement;
    }
  };

  componentDidMount() {
    this.initRootContainer();

    if (rootElement) {
      rootElement.appendChild(this.el);
    }
  }

  componentWillUnmount() {
    if (rootElement) {
      rootElement.removeChild(this.el);
      rootElement = null;
    }
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el);
  }
}

export default ModalPortal;
