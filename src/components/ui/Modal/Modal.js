import { useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { useRefEffect } from '../../../hooks/useRefEffect';
import ModalPortal from './ModalPortal';
import styles from './Modal.module.css';

function Modal({
  isOpen,
  onRequestClose,
  overlayClassName,
  className,
  onAfterOpen,
  children,
  shouldDisableScroll,
}) {
  useEffect(() => {
    isOpen && onAfterOpen && onAfterOpen();
  }, [isOpen, onAfterOpen]);

  const modalRef = useRefEffect((node) => {
    if (isOpen && shouldDisableScroll) {
      disableBodyScroll(node);
    }
    return () => enableBodyScroll(node);
  });

  if (!isOpen || !children) {
    return null;
  }

  return (
    <ModalPortal>
      <div
        className={classnames(styles.overlay, overlayClassName)}
        onClick={onRequestClose}
      >
        <div
          ref={modalRef}
          className={classnames(styles.modal, className)}
          onClick={(e) => e.stopPropagation()}
        >
          {children}
        </div>
      </div>
    </ModalPortal>
  );
}

Modal.propTypes = {
  isOpen: PropTypes.bool,
  className: PropTypes.string,
  onAfterOpen: PropTypes.func,
  onRequestClose: PropTypes.func,
  overlayClassName: PropTypes.string,
  children: PropTypes.node,
  shouldDisableScroll: PropTypes.bool,
};

Modal.defaultProps = {
  isOpen: false,
  shouldDisableScroll: true,
};

export default Modal;
