import { ReactNode } from 'react';
import styles from './Button.module.css'

type Props = {
  children: ReactNode;
  onClick?: () => void;
};

export default function Button({ children, ...rest }: Props) {
  return (
    <button className={styles.root} {...rest}>{children}</button>
  )
}
