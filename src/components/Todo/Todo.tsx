import React from 'react';
import classNames from 'classnames';
import styles from './Todo.module.css';

export default function Todo() {
  return (
    <div className={styles.root}>
      <div className={styles.arm}></div>

      <div className={styles.center}>
        <div className={styles.head}></div>
        <div className={styles.body}></div>
        <div className={styles.legs}>
          <div className={styles.leg}></div>
          <div className={styles.leg}></div>
        </div>
      </div>

      <div className={classNames(styles.arm, styles.right)}></div>
    </div>
  )
}
