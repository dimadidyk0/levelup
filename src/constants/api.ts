export const API_PREFIX: string = '';

export const SELF_STORAGE_API_URL = `${API_PREFIX}/self-storage`;