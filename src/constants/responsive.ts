export const isDesktopLayout: { minWidth: number } = { minWidth: 1280 };
export const isMobileLayout: { maxWidth: number } = { maxWidth: 1279 };
