export const ROOT = '/';
export const PROFILE = '/profile';
export const SETTINGS = '/settings';
export const FINANCE = '/finance';
export const CALENDAR = '/calendar';
export const TODO = '/todo';

export const SELF_STORAGE = '/self-storage';
export const SELF_STORAGE_ADD = `${SELF_STORAGE}/add`;
