import { useMediaQuery } from 'react-responsive';
import { isDesktopLayout, isMobileLayout } from '../constants/responsive';

export function useIsDesktop(): boolean {
  return useMediaQuery(isDesktopLayout);
};

export function useIsMobile(): boolean {
  return useMediaQuery(isMobileLayout);
};
