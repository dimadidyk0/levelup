import { useCallback, useRef } from 'react';

export function useRefEffect(callback) {
  const currentCallback = useRef(callback);
  // WARNING: Mutating a ref's value as a side effect of the render function
  // being called is probably a big anti-pattern, and might cause bugs
  //
  // However, useEffect and useLayoutEffect (which we would normally use for
  // updating a ref to the latest value) get triggered before callback refs.
  // Just mutating it in the render function is the only way I could think of to
  // update the value before callback ref is triggered by React.
  currentCallback.current = callback;

  const cleanupRef = useRef(null);

  const callbackRef = useCallback((node) => {
    if (node) {
      cleanupRef.current = currentCallback.current(node) || null;
    } else if (cleanupRef.current) {
      cleanupRef.current();
      cleanupRef.current = null;
    }
  }, []);

  return callbackRef;
}
