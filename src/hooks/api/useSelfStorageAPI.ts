import  { useEffect, useState } from 'react';
import { SELF_STORAGE_API_URL } from '../../constants/api';

export function useFetchSelfStorage(): { data: any[]; isLoading: boolean, error: null | Object } {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    setIsLoading(true);

    fetch(SELF_STORAGE_API_URL)
      .then((r) => r.json())
      .then((data) => {
        setNotes(data);
        setIsLoading(false);
      })
      .catch(err => setError(err));
  }, []);

  return { data: notes, isLoading, error }
}
