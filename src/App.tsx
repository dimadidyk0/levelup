import React from 'react';
import { Routes, Route } from "react-router-dom";
import Main from './pages/Main/Main';
import Profile from './pages/Profile/Profile';
import Todo from './pages/Todo/Todo';
import SelfStorage from './pages/SelfStorage/SelfStorage';
import SelfStorageAdd from './pages/SelfStorageAdd/SelfStorageAdd';
import './assets/styles/variables.css';
import './App.css';
import { PROFILE, ROOT, SELF_STORAGE, SELF_STORAGE_ADD, TODO, FINANCE, CALENDAR, SETTINGS } from './constants/routes';

function App() {
	return (
    <Routes>
      <Route path={ROOT} element={<Main />} />
      <Route path={PROFILE} element={<Profile />} />
      <Route path={TODO} element={<Todo />} />

      <Route path={SELF_STORAGE} element={<SelfStorage />} />
      <Route path={SELF_STORAGE_ADD} element={<SelfStorageAdd />} />

      {/* TODO: set 404 instead of main. Tempo solution */}
      <Route path="*" element={<Main />} />
    </Routes>
	);
}

export default App;
