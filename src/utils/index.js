function matchesK(K, inputArray) {
	const map = {}
	inputArray.forEach(number => map[number] = 1);

	inputArray.reduce((acc, el) => {
		Object.keys(map).forEach(key => {
			const nextValue = +key + +el;
			acc[nextValue] = 1;
		});

		return acc;
	}, map);

	return map[K] ? "Yes" : "No";
}

function g(element, index, array, map) {
	map[element] = 1;
	const copy = [...array];
	copy.slice(index, index + 1);

	copy.forEach(el => {
		Object.keys(map).forEach(key => {
			const nextValue = +key + +el;
			map[nextValue] = 1;
		});

		
	});
}

matchesK(4, [1,1,2,4,8]) // true
// matchesK(5, [1,1,2,4,8]) // true
// matchesK(3, [1,1,2,4,8]) // true
// matchesK(9, [1,1,2,4,8]) // true
// matchesK(0, [1,1,2,4,8]) // false
// matchesK(40, [1,1,2,4,8]) // false